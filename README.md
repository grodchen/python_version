#### Instructions

Run tests:

    python -m unittest

Run program:

    python console_app1.py FILENAME
    (-h flag for help)

Alternative program (bash commandline):

    grep -o "apa" apa.txt | wc -l


#### Assumptions

* When file does not exist, or a file not provided, a warning should be shown and fail code returned
* utf-8 support will be enough localization, we are not prepared to handle e.g. chinese.
* All single, non-overlapping occurrences of the string should match.
  No effort is made to make sure it is a complete word.