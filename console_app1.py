import sys
import re
import os
import argparse

desc = ('Count number of times the filename (without ext) is repeated in the file.')

def main(raw_args):
    args = parse_args(raw_args)
    if not os.path.exists(args.filename):
        print("File does not exist: " + args.filename)
        sys.exit(1)
    count_filename(args.filename)

def parse_args(raw_args):
    parser = argparse.ArgumentParser(description=desc)
    parser.add_argument('filename', metavar='FILENAME')
    args = parser.parse_args(raw_args)
    return args

def count_filename(filename):
    name = os.path.splitext(os.path.basename(filename))[0]

    with open(filename, encoding='utf-8') as file:
        cnt = sum([len(re.findall(name, line)) for line in file.readlines()])
    print("found " + str(cnt))

if __name__ == "__main__":
    main(sys.argv[1:])
