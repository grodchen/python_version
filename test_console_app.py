# -*- coding: utf-8 -*-

import console_app1

from io import StringIO
import os
from os.path import join
from parameterized.parameterized import parameterized
import unittest
from unittest import mock
import tempfile


@mock.patch('builtins.print')
class TestConsoleApp(unittest.TestCase):
    @parameterized.expand([
        ['no_match', 'x.txt', 'a\nb\n', 'found 0'],
        ['1_match', 'x.txt', 'a\nx\n', 'found 1'],
        ['3_matches', 'apa.txt', 'apa\naappx\napa\napa','found 3'],
        ['empty_file', 'x.txt', '',  'found 0'],
        ['two_mathces_on_line','apa.txt', 'apa apa', 'found 2'],
        ['no_filename_ext', 'apa', 'apa\napa\napa', 'found 3'],
        ['filename_has_subdir', 'subdir/apa.txt', 'apa apa\n apa', 'found 3'],
        ['non_ascii_filename', 'häj.txt', 'häjhäj', 'found 2'],
        ['filename_w_whitespace', 'a file.txt', 'this is a file', 'found 1']
        ])
    def test_match(self, mock_print, _descr, filename, text, expected):
        with FileInTempDir(filename, text):
            console_app1.main([filename])
        mock_print.assert_called_with(expected)

    def test_file_does_not_exist(self, mock_print):
        with self.assertRaises(SystemExit) as e:
            console_app1.main(['unknown_file.txt'])
        mock_print.assert_called_with('File does not exist: unknown_file.txt')

    @mock.patch('sys.stderr', new_callable=StringIO)
    def test_no_cmdline_arguments(self, mock_stderr, mock_print):
        with self.assertRaises(SystemExit) as e:
            console_app1.main([])
        self.assertIn('the following arguments are required: FILENAME',
                      mock_stderr.getvalue())


class FileInTempDir(tempfile.TemporaryDirectory):
    def __init__(self, filename, text):
        self.filename = filename
        self.text = text
        super(FileInTempDir, self).__init__()

    def __enter__(self):
        ret = super(FileInTempDir, self).__enter__()
        tmpdir = self.name
        self.currdir = os.getcwd()
        os.chdir(tmpdir)
        subdir = os.path.dirname(self.filename)
        if subdir:
            os.makedirs(subdir)
        with open(self.filename, 'w', encoding='utf-8') as f:
            f.write(self.text)
        return ret

    def __exit__(self, *args, **kwargs):
        os.chdir(self.currdir)
        super(FileInTempDir, self).__exit__(*args, **kwargs)

